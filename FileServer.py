import socket, threading, os

def RetrieveFile(name, sock):
	filename = sock.recv(1024)
	print(filename)
	if os.path.isfile(filename):
		message = "EXISTS " + str(os.path.getsize(filename))
		sock.send(message.encode('utf-8'))
		userResponse = sock.recv(1024)
		if userResponse[:2].decode() == 'OK':
			with open(filename, 'rb') as file:
				bytesToSend = file.read(1024)
				sock.send(bytesToSend)
				while bytesToSend != "":
					bytesToSend = file.read(1024)
					sock.send(bytesToSend)
	else:
		err = "ERROR"
		sock.send(err.encode('utf-8'))
	sock.close()

def Main():
	host = '127.0.0.1'
	port = 5000

	sock = socket.socket()
	sock.bind((host, port))

	sock.listen(5)

	print("Server Started...")
	while True:
		connection, address = sock.accept()
		print("client connected ip:<" + str(address) + ">")

		thread = threading.Thread(target=RetrieveFile, args=('retrThread', connection))
		thread.start()
	sock.close()



if __name__ == '__main__':
	Main()