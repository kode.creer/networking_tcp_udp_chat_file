import socket

def Main():
	host = '127.0.0.1'
	port = 5000

	sock = socket.socket()

	sock.connect((host, port))

	filename = input("Filename? ->")

	if filename != 'q':
		sock.send(filename.encode('utf-8'))
		data = sock.recv(1024)
		print(data)
		if data[:6].decode() == 'EXISTS':
			filesize = int(data[6:])
			message = input('File Exists ' + str(filesize) +\
			 ', Want to download? (Y/N)')

			if message == 'Y':
				ok = 'OK'
				sock.send(ok.encode('utf-8'))
				file = open('new_'+filename, 'wb')
				data = sock.recv(1024)
				totalRecieved = len(data)
				file.write(data)

				while totalRecieved < filesize:
					data = sock.recv(1024)
					print(data)
					totalRecieved += len(data)
					file.write(data)
					#to 2 decimal points
					percentage = totalRecieved/float(filesize)*100
					print("{0}".format(str(percentage)) + "% Done")
				print("Download complete")
				#file.close()
		else:
			print("File does not Exists")
			print(filename)
		sock.close()



if __name__ == '__main__':
	Main()
























