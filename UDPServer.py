import socket

def Main():
	host = '127.0.0.1'
	port = 5000

	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	sock.bind((host, port))

	print("Server Started")
	while True:
		data, address = sock.recvfrom(1024)
		print("Message from: " + str(address))
		print("From connect user: " + str(data))
		data = str(data).upper()
		print("Sending: " + str(data))
		sock.sendto(data.encode('utf-8'), address)
	sock.close()

if __name__ == "__main__":
	Main()