import socket

def Main():
	#we want the host to be this machine
	host = '127.0.0.1'
	port =  5000
	sock = socket.socket()
	sock.bind((host, port))

	sock.listen(1)

	conection, address = sock.accept()

	print("Connection from : " + str(address))
	while True:
		#how many buffer bytes
		data = conection.recv(1024)
		#if the connection is close
		if not data:
			break
		print("from connected user: " + str(data))
		data = str(data).upper()
		print("sending: " + str(data))
		conection.send(data.encode('utf-8'))
	conection.close()

if __name__ == '__main__':
	Main()